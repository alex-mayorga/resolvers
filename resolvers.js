const doh = require('./doh')
const dns = require('./dns')

module.exports.doh = doh
module.exports.dns = dns

module.exports.aliased = {
  doh: new Map(doh.map(({ alias, name, doh }) => [alias, { name, doh }])),
  dns: new Map(dns.map(({ alias, name, dns4, dns6 }) => [alias, { name, dns4, dns6 }]))
}
