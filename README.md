# resolvers

List of public DNS over HTTPS servers.

Extracted from Commons Host projects [Dohnut](https://www.npmjs.com/package/dohnut) and [Bulldohzer](https://www.npmjs.com/package/bulldohzer).
